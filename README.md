# assignment-5-shanks2710
# Assignment 5
This project consists of methods for the following tasks in JavaScript:
1) Search an element in an array using Binary-Search
2) Sort an array using Bubble-Sort algorithm
3) Check if a string is a palindrome or not
Additionally the project contains documentation using the ESDoc comments.

# Prerequisites
This project requires npm to install plugins for ESDoc and to run commands to generate the documentation using ESDoc.
It uses features of JavaScript and can be run on any online/Offline editor including ,but not limited to, Repl.it, JSfiddle, Microsoft Visual Studio Code.

# Built With
Visual Source Code - IDE to create project.
ESDoc: Used to generate documentation for the project

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-5-shanks2710
2) Open Visual Source Code and Select File->Open Project
3) Navigate to the respective folder
4) Go to Terminal Section
5) Navigate to src folder with command: cd src
6) Run the command to run the project: node assignment5.js
8) Run the command to generate the documentation via ESDoc plugin: node run doc

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs
