// Assignment-5

//FUNCTIONS START
//Function for Binary Search
/**
   * @param {!number[]} _arr - this is array param that contains the array to be searched for the element.
   * @param {!number} searchVal - this is the value to be searched in the array.
   * @param {!number} start - this is the starting index of the array for the binary-search
   * @param {!number} end - this is the last index of the array for the binary-search
   */
function binarySearchRecursive(_arr, searchVal, start, end) {
    if (start > end) {
        console.log('Item: ' + searchVal + 'does not exist in Array');
        return;
    }

    let mid = Math.floor((start + end) / 2);
    if (searchVal == _arr[mid]) {
        console.log('Item: ' + searchVal + ' exists in Array');
    } else if (_arr[mid] > searchVal) {
        binarySearchRecursive(_arr, searchVal, start, mid - 1);
    } else {
        binarySearchRecursive(_arr, searchVal, mid + 1, end);
    }
}

// Function for Bubble-Sort
/**
   * @param {!number[]} unsortedArr - this is array param that contains the array to be sorted using Bubble-Sort algorithm.
   * @return {number{}} - this is the array that has been sorted using Bubble-Sort algorithm
   */
function bubbleSort(unsortedArr) {
    let sortFlg = false;
    let lastIndex = unsortedArr.length - 1;
    while (!sortFlg) {
        for (let i = 0; i < lastIndex; i++) {
            sortFlg = true;
            if (unsortedArr[i] > unsortedArr[i + 1]) {
                [unsortedArr[i], unsortedArr[i + 1]] = [unsortedArr[i + 1], unsortedArr[i]];
                sortFlg = false;
            }
        }
        lastIndex--;
    }
    return unsortedArr;
}

// Function to check palidrome
/**
   * @param {!string} str - this is String param that needs to be checked if it is a paindrome or not.
   */
function palindromeCheck(str) {
    let regex = /[^A-Za-z0-9_]/g;
    let lowerStr = str.toLowerCase().replace(regex, "");
    let reversedStr = lowerStr.split("").reverse().join("");
    reversedStr === lowerStr ? console.log(str + " is a Palindrome") : console.log(str + " is not a Palindrome");
}
//FUNCTIONS END

//PROGRAM RUN STARTS
/**
     * @type {number[]} - Unsorted Array of numbers
     */
let arrBubSort = [1, 9, 100, 240, 12, 0, 200, 180, 90];
console.log("Unsorted Array: " + arrBubSort);
/**
     * @type {number[]} - Sorted Array of numbers
     */
let sortedArr = bubbleSort(arrBubSort);

console.log("Search : " + 12);
binarySearchRecursive(sortedArr, 12, 0, sortedArr.length - 1);

console.log("Sorted Array: " + sortedArr);

palindromeCheck('racecar');

////PROGRAM RUN ENDS
